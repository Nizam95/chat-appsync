import 'dart:convert';

import 'package:demo_app/client.dart';
import 'package:demo_app/custom_link.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;
import 'package:flutter_chat_ui/flutter_chat_ui.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:mime/mime.dart';
import 'package:open_file/open_file.dart';
import 'package:uuid/uuid.dart';

String toBase64(Map data) => base64.encode(utf8.encode(jsonEncode(data)));

void main() {
  initializeDateFormatting().then((_) {
    runApp(const MyApp());
  });
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ChatPage(),
    );
  }
}

class ChatPage extends StatefulWidget {
  const ChatPage({Key? key}) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  List<types.Message> _messages = [];
  final _user = const types.User(id: '06c33e8b-e835-4736-80f4-63f44b66666c');

  ///start
  ///to add const env here
  static const appSyncHostUrl = '';
  static const appSyncHttpApi = '';
  static const appSyncRealtimeApi = '';
  /// user current login token
  static const userToken = '';

  /// add user token Authorization
  static const authHeaders = {
    "host": appSyncHostUrl,
    "Authorization": 'Bearer $userToken'
  };

  /// create Auth headers parameter
  CustomAuthLink authLink = CustomAuthLink(getHeaders: () => authHeaders);

  /// initialize httpLink & websocketLink variable
  late HttpLink httpLink;
  late GraphQLClient httpClient;
  late WebSocketLink webSocketLink;
  late GraphQLClient webScoketClient;

  /// [subscribe parameter] - subscribe realtime data update (for user to receive new data automatically)
  final String onCreateMessage = """
  subscription OnCreateMessage {
    subscribeChannel {
      channelId
      content
      createdAt
      deleted
      deletedAt
      deletedBy
      id
      metadata {
        mimeType
        name
        size
        thumbnailUrl
        uri
      }
      starred
      status
      tenantId
      type
      updatedAt
      userId
      thread {
        parentMessageId
        type
      }
      seenBy {
        avatarUri
        id
        name
        readTimestamp
        tenantId
        userId
      }
    }
  }
   """;

  /// [mutate parameter] - to create/update/delete data
  static const text = "Hello world";

  /// user input text
  final String createMessage = """
  mutation CreateMessage {
    createMessage(channelId: "134_777_public", content: "$text", type: text, starred: false) {
      channelId
      content
      createdAt
      deleted
      deletedAt
      deletedBy
      id
      seenBy {
        avatarUri
        id
        readTimestamp
        name
        tenantId
        userId
      }
      metadata {
        mimeType
        name
        size
        thumbnailUrl
        uri
      }
      starred
      status
      tenantId
      thread {
        parentMessageId
        type
      }
      type
      updatedAt
      userId
    }
  }
   """;

  /// [query parameter] - to get list of data
  final getMessages = """
  query GetMessages {
    getMessages {
      channelId
      content
      createdAt
      deleted
      deletedAt
      deletedBy
      id
      metadata {
        mimeType
        name
        size
        thumbnailUrl
        uri
      }
      seenBy {
        avatarUri
        id
        name
        readTimestamp
        tenantId
        userId
      }
      starred
      status
      type
      updatedAt
      userId
      tenantId
      thread {
        parentMessageId
        type
      }
    }
  }
  """;

  ///end

  @override
  void initState() {
    super.initState();

    ///start
    /// 1. initialize websocket connection configuration

    /// http api (for query & mutate)
    httpLink = HttpLink(appSyncHttpApi);

    /// websocket realtime api (for subscription)
    /// encode authHeaders to base64
    final encodedHeader = toBase64(authHeaders);
    webSocketLink = WebSocketLink(
      '$appSyncRealtimeApi?header=$encodedHeader&payload=e30=',
      config: const SocketClientConfig(
          autoReconnect: false,
          inactivityTimeout: Duration(minutes: 5),
          serializer: AppSyncRequest(authHeader: authHeaders)),
    );

    /// 2. initialize graphql client
    /// httpClient (for query & mutate)
    final httpClient = GraphQLClient(
        link: authLink.concat(httpLink), //appending
        cache: GraphQLCache(),
        alwaysRebroadcast: true);

    /// websocketClient (for subscription)
    final websocketClient = GraphQLClient(
        link: authLink.concat(webSocketLink), //appending
        cache: GraphQLCache(),
        alwaysRebroadcast: true);

    ///end

    ///start
    /// using synchronous call
    ///
    /// [Subscribe] - example for subscribing realtime message update
    // websocketClient
    //     .subscribe(SubscriptionOptions(document: gql(onCreateMessage)))
    //     .listen((result) {
    //   print(result);
    //
    //   /// if success
    //   if (!result.hasException) {
    //     // MessageModel messageModels = MessageModel.fromSingleJson(
    //     //     {"data": result.data?['onCreateMessage']});
    //     //
    //     // _addMessage(messageModels.data[0]);
    //   } else {
    //     /// popup error message or do something
    //   }
    // });

    /// [Mutate] - example for create new message
    // httpClient
    //     .mutate(MutationOptions(document: gql(createMessage)))
    //     .then((result) {
    //   /// if success
    //   if (!result.hasException) {
    //     /// map to model & show to user
    //     print(result.data);
    //   } else {
    //     print(result.exception.toString());
    //
    //     /// popup error message or do something
    //   }
    // });

    /// [Query] - example for get message list
    // httpClient.query(QueryOptions(document: gql(getMessages))).then((result) {
    //   /// if success
    //   if (!result.hasException) {
    //     /// map to model & show to user
    //     print(result.data);
    //   } else {
    //     print(result.exception.toString());
    //
    //     /// popup error message or do something
    //   }
    // });

    ///end

    _loadMessages();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: Chat(
          messages: _messages,
          onAttachmentPressed: _handleAtachmentPressed,
          onMessageTap: _handleMessageTap,
          onPreviewDataFetched: _handlePreviewDataFetched,
          onSendPressed: _handleSendPressed,
          user: _user,
        ),
      ),
    );
  }

  void _addMessage(types.Message message) {
    setState(() {
      _messages.insert(0, message);
    });
  }

  void _handleAtachmentPressed() {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return SafeArea(
          child: SizedBox(
            height: 144,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    _handleImageSelection();
                  },
                  child: const Align(
                    alignment: Alignment.centerLeft,
                    child: Text('Photo'),
                  ),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    _handleFileSelection();
                  },
                  child: const Align(
                    alignment: Alignment.centerLeft,
                    child: Text('File'),
                  ),
                ),
                TextButton(
                  onPressed: () => Navigator.pop(context),
                  child: const Align(
                    alignment: Alignment.centerLeft,
                    child: Text('Cancel'),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _handleFileSelection() async {
    final result = await FilePicker.platform.pickFiles(
      type: FileType.any,
    );

    if (result != null && result.files.single.path != null) {
      final message = types.FileMessage(
        author: _user,
        createdAt: DateTime.now().millisecondsSinceEpoch,
        id: const Uuid().v4(),
        mimeType: lookupMimeType(result.files.single.path!),
        name: result.files.single.name,
        size: result.files.single.size,
        uri: result.files.single.path!,
      );

      _addMessage(message);
    }
  }

  void _handleImageSelection() async {
    final result = await ImagePicker().pickImage(
      imageQuality: 70,
      maxWidth: 1440,
      source: ImageSource.gallery,
    );

    if (result != null) {
      final bytes = await result.readAsBytes();
      final image = await decodeImageFromList(bytes);

      final message = types.ImageMessage(
        author: _user,
        createdAt: DateTime.now().millisecondsSinceEpoch,
        height: image.height.toDouble(),
        id: const Uuid().v4(),
        name: result.name,
        size: bytes.length,
        uri: result.path,
        width: image.width.toDouble(),
      );

      _addMessage(message);
    }
  }

  void _handleMessageTap(types.Message message) async {
    if (message is types.FileMessage) {
      await OpenFile.open(message.uri);
    }
  }

  void _handlePreviewDataFetched(
    types.TextMessage message,
    types.PreviewData previewData,
  ) {
    final index = _messages.indexWhere((element) => element.id == message.id);
    final updatedMessage = _messages[index].copyWith(previewData: previewData);

    WidgetsBinding.instance?.addPostFrameCallback((_) {
      setState(() {
        _messages[index] = updatedMessage;
      });
    });
  }

  void _handleSendPressed(types.PartialText message) {
    final textMessage = types.TextMessage(
      author: _user,
      createdAt: DateTime.now().millisecondsSinceEpoch,
      id: const Uuid().v4(),
      text: message.text,
    );

    _addMessage(textMessage);
  }

  void _loadMessages() async {
    final response = await rootBundle.loadString('assets/messages.json');
    final messages = (jsonDecode(response) as List)
        .map((e) => types.Message.fromJson(e as Map<String, dynamic>))
        .toList();

    setState(() {
      _messages = messages;
    });
  }
}
