import 'package:flutter_chat_types/flutter_chat_types.dart' as types;

class MessageModel {
  List<types.Message> data = <types.Message>[];

  MessageModel({required this.data});

  MessageModel.fromSingleJson(dynamic json) {
    if (json['data'] != null) {
      var v = json['data'];
      v['id'] = v['userId'];

      if (v['userId'] == 'b4878b96-efbc-479a-8291-474ef323dec7') {
        v["author"] = {
          "firstName": "John",
          "id": "b4878b96-efbc-479a-8291-474ef323dec7",
          "imageUrl": "https://avatars.githubusercontent.com/u/14123304?v=4"
        };
      } else if (v['userId'] == '06c33e8b-e835-4736-80f4-63f44b66666c') {
        v["author"] = {
          "firstName": "Jane",
          "id": "06c33e8b-e835-4736-80f4-63f44b66666c",
          "imageUrl": "https://avatars.githubusercontent.com/u/33809426?v=4"
        };
      }

      data.add(types.Message.fromJson(v));
    }
  }

  MessageModel.fromJson(dynamic json) {
    if (json['data'] != null) {
      json['data'].forEach((v) {
        v['id'] = v['userId'];

        if (v['userId'] == 'b4878b96-efbc-479a-8291-474ef323dec7') {
          v["author"] = {
            "firstName": "John",
            "id": "b4878b96-efbc-479a-8291-474ef323dec7",
            "imageUrl": "https://avatars.githubusercontent.com/u/14123304?v=4"
          };
        } else if (v['userId'] == '06c33e8b-e835-4736-80f4-63f44b66666c') {
          v["author"] = {
            "firstName": "Jane",
            "id": "06c33e8b-e835-4736-80f4-63f44b66666c",
            "imageUrl": "https://avatars.githubusercontent.com/u/33809426?v=4"
          };
        }

        data.add(types.Message.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['data'] = this.data.map((v) => v.toJson()).toList();
    return data;
  }
}
